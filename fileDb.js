const fs = require('fs');
const nanoid = require("nanoid");

module.exports = {
    init: callback => {
        const path = './dbFolder';
        const filePaths = [];
        const content = [];

        fs.readdir(path, (err, files) => {
            files.forEach(file => {
                const filePath = `${path}/${file}`;
                filePaths.push(filePath);
            });
            // console.log(filePaths, 'FILE PATHS');
            const lastFive = filePaths.slice(-5);
            lastFive.forEach(filePath => {
                const data = fs.readFileSync(filePath, "utf8");
                content.push(JSON.parse(data))
            });
            // console.log(content, 'CONTENT');

            data = JSON.stringify(content);
            callback();
        });
    },
    getData: () => data,

    addItem: (item, callback) => {
        item.id = nanoid();
        fileName = Date.now();
        item.date = new Date();
        const dbFile = `./dbFolder/${fileName}.txt`;
        let content = JSON.stringify({message: item.message, date: item.date}, null, 2);

        fs.writeFile(dbFile, content, err => {
            if (err) throw err;
            console.log('File saved');
        });
        callback(content);
    }
};