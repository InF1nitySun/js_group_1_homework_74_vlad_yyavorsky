const express = require('express');
const fileDb = require('./fileDb');
const products = require('./app/products');
const app = express();

const port = 8000;

app.use(express.json());

fileDb.init(() => {
    console.log('Database file was loaded!');

    app.use('/products', products(fileDb));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
